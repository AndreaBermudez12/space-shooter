﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{

    Vector2 speed;
    public GameObject[] graphics;
    int seleccionado;
    public ParticleSystem ps;

    public AudioSource audioSource;

    public GameObject meteorToInstanciate;

    private void Awake()
    {
        for (int i = 0; i < graphics.Length; i++)
        {

            graphics[i].SetActive(false);


        }

        seleccionado = Random.Range(0, graphics.Length);
        graphics[seleccionado].SetActive(true);

        speed.x = Random.Range(-4, -1);
        speed.y = Random.Range(-4, 5);

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);

        graphics[seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);

    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(gameObject);
        
        }else if(other.tag == "Bullet") {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        graphics[seleccionado].SetActive(false);

        //Desactivo el box collider
        Destroy(GetComponent<BoxCollider2D>());


        //Lanzo sonido de explosion
        audioSource.Play();

        ps.Play();

        //Instanciamos meteoritos medianos
        InstanceMeteors();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);

    }

    public virtual void InstanceMeteors()
    {
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }
}
