﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship_Input : MonoBehaviour
{
    public Vector2 axis;

    public Player_Behaviour ship;

    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

        if(Input.GetButton("Fire1"))
        {
            ship.Shoot();
            Debug.Log("Fire Nave");
        }

        if (Input.GetButton("Fire2"))
        {
            ship.Shoot();
            Debug.Log("Fire Nave");
        }

        //Debug.Log("x:"+axis.x + "y:" +axis.y);

        ship.SetAxis(axis);
    }


}
