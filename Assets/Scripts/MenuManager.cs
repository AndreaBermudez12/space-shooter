﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    public void PulsaPlay()
    {
        Debug.LogError("He pulsado play");

        SceneManager.LoadScene("Game");
    }

    public void PulsaCreditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }
}
