﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon6 : Weapon
{

    public GameObject laserBullet_6;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(laserBullet_6, this.transform.position, Quaternion.identity, null);
    }
}
