﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Behaviour : MonoBehaviour
{
    public Vector2 limits;
    public float speed;
    private Vector2 axis;

    public float shootTime = 0;
    public Weapon weapon;

    public Propeller prop;

    public AudioSource audioSource;

    public ParticleSystem ps;
    int seleccionado;
    public GameObject graphics;
    public BoxCollider2D collider;

    public ScoreManager scoreManager;


    public int lives = 3;
    private bool iamDead = false;


    void Update()
    {
        if (iamDead)
        {
            return;
        }

        shootTime += Time.deltaTime;
        transform.Translate(axis * speed * Time.deltaTime);

        

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }

        if (axis.x > 0) 
        {
            prop.BlueFire();
        } 
        else
        {
            prop.Stop();
        }

        if (axis.x < 0)
        {
            prop.RedFire();
        }
        else
        {
            prop.Stop2();
        }
    }


    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;
    }

    public void SetAxis(float x, float y)
    {
        axis = new Vector2(x, y);
    }

    public void Shoot()
    {
        if(shootTime>weapon.GetCadencia())
        {
            weapon.Shoot();
            shootTime = 0;
        }

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Meteor")
        {
            StartCoroutine(DestroyShip());
        }
    }

    IEnumerator DestroyShip()
    {
        //Indico que estoy muerto
        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Lanzo sonido de explosion
        audioSource.Play();

        ps.Play();

        collider.enabled = false;

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //score
        scoreManager.LoseLife();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        //Destroy(this.gameObject);

        if(lives>0)
        {
            StartCoroutine(inMortal());
        }


    }

    IEnumerator inMortal()
    {
        iamDead = false;
        graphics.SetActive(true);

        prop.gameObject.SetActive(true);

        for(int i=0;i<15;i++)
        {
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);

        }

        collider.enabled = true;

    }


    /*
    public void Shoot()
    {
        if (shootTime>weapon.GetCadencia())
        {
            weapon.Shoot();
            shootTime = 0;
        }

    }
    */

}
